import csv
import os

import make_sig_files
import add_to_DB_tables

import sys
import nltk
from collections import defaultdict


# Set allowed code extensions
ALLOWED_EXTENSIONS = set(['txt', 'js', 'py', 'rb', 'java', 'cpp', 'c'])


with open('c_repos.csv', 'rb') as csvfile:
	reader = csv.reader(csvfile, delimiter=',', quotechar='|')
	reader.next()

	num_repos_to_clone = 10;

	for i in range(num_repos_to_clone):
		repoURL = reader.next()[1]
		print "----------------"
		print repoURL

		splitURL = repoURL.split('/')
		repoName = splitURL[len(splitURL)-1]

		## Clones the repo to disk
		os.system("git clone " + repoURL)
		#os.system("git clone " + repoURL + " output")

		## API1, API2
		#make_sig_files.make_files()
		#add_to_DB_tables.make_DB(repoURL) #error :(

		## Clear the code from disk

		#os.system("rm -rf ./output")
		## Clear the sig files from disk
		#os.system("rm -rf ./sig_files/java/*")
		#os.system("rm -rf ./sig_files/c/*")
		#os.system("rm -rf ./sig_files/cpp/*")
		#os.system("rm -rf ./sig_files/py/*")


def generateNGramDictForRepo(username, rootdir):

	fileList = []
	fileSize = 0
	folderCount = 0
	user = username


	my_dict = defaultdict(list)

	for root, subFolders, files in os.walk(rootdir):
		folderCount += len(subFolders)
		for file in files:
			f = os.path.join(root,file)
			fileSize = fileSize + os.path.getsize(f)
			#print(f)
			fileList.append(f)

			if '.' in f and f.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS: ## Check file extensions
				fileKey = user + "/" + f #Generate the key
				#print fileKey

				codeFile = open(f, 'rb') #Open the file
				codeStr = codeFile.read()
				#print codeStr
				
				codeStrNGrams = nltk.util.ngrams(codeStr, 5) #Generate the NGrams

				new_dict = defaultdict(list)
				for key in codeStrNGrams:
					new_dict[key].append([fileKey])

					for key,value in new_dict.iteritems():
						my_dict[key].extend(value)

				codeFile.close()

	# my_dict has all the ngram to list of filekeys for every python 
	return my_dict

username = "DAPME_Lab"
rootdir = "TBWA"

#my_dict = generateNGramDictForRepo(username, rootdir)
#print len(my_dict)

