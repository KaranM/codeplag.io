'''
Creates the DynamoDB tables for 4 languages. Can be customized to include any number of languages.
Note : MUST only be run ONCE for this application
Keep in mind the names of the tables created.
Must have AWS Access Key etc. in boto config files
'''
import boto.dynamodb2
from boto.dynamodb2.fields import HashKey, RangeKey, KeysOnlyIndex, AllIndex
from boto.dynamodb2.table import Table

def main():
	# take note of the name, schema of the created table
	dumdum = Table.create('index_c', schema=[HashKey('hash_value')], throughput={'read': 50,'write': 20}, connection=boto.dynamodb2.connect_to_region('us-east-1'))
	dumdum = Table.create('index_cpp', schema=[HashKey('hash_value')], throughput={'read': 50,'write': 20},connection=boto.dynamodb2.connect_to_region('us-east-1'))
	dumdum = Table.create('index_java', schema=[HashKey('hash_value')], throughput={'read': 50,'write': 20},connection=boto.dynamodb2.connect_to_region('us-east-1'))
	dumdum = Table.create('index_python', schema=[HashKey('hash_value')], throughput={'read': 50,'write': 20},connection=boto.dynamodb2.connect_to_region('us-east-1'))

if  __name__ =='__main__':
	main()
