'''
Reads in a bunch of files containing intermediate code representation of a single github repo.
Stores these as inverted index in DynamoDB tables using a unique file names
'''
'''
TODO : Code cleanup! quite non-pythonic right now :(
'''
import boto.dynamodb2
from boto.dynamodb2.fields import HashKey, RangeKey, KeysOnlyIndex, AllIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.items import Item
import nltk
from os import listdir
from os.path import isfile, join
from collections import defaultdict
import time
import sys,os
import subprocess
from subprocess import check_output
from subprocess import Popen, PIPE

# tables are global variable... yeah...
index_c = Table('index_c')
index_cpp = Table('index_cpp')
index_java = Table('index_java')
index_py = Table('index_python')

# global error count variable :(
global_errs = 0

def main():
	make_db("github.com/prakhar1989/Algorithms.git")
	
def make_db(git_repo_name):
	
	t1 = time.time()
	mypath_java = './sig_files/java/' # path to dir containg sig files of java code 
	mypath_c = './sig_files/c/' # path to dir containg sig files of java code 
	mypath_cpp = './sig_files/cpp/' # path to dir containg sig files of java code 
	mypath_py = './sig_files/py/' # path to dir containg sig files of java code 
	# Note - every file needs a global file identifier
	# should be created from the name of the github repo as : username_reponame_filename
	repo_split = git_repo_name.strip().split("/")
	git_split = repo_split[2].strip().split(".git")
	
	global_id = repo_split[1]+"&"+git_split[0]+"&" # append filename here
	
	onlyfiles_java = [ join(mypath_java,f) for f in listdir(mypath_java) if isfile(join(mypath_java,f)) ] # list of all java sig files 
	onlyfiles_c = [ join(mypath_c,f) for f in listdir(mypath_c) if isfile(join(mypath_c,f)) ] # list of all c sig files 
	onlyfiles_cpp = [ join(mypath_cpp,f) for f in listdir(mypath_cpp) if isfile(join(mypath_cpp,f)) ] # list of all cpp sig files 
	onlyfiles_py = [ join(mypath_py,f) for f in listdir(mypath_py) if isfile(join(mypath_py,f)) ] # list of all py sig files 
	
	# creating dicts for hash codes locally... will copy these to DB once done with this repo
	java_dict = defaultdict(list)
	c_dict = defaultdict(list)
	cpp_dict = defaultdict(list)
	py_dict = defaultdict(list)

	t2 = time.time()
	print "Preliminary stuff took %s seconds." %(str(t2-t1))
	
	# generate a list of all words in every java file...
	for each_file in onlyfiles_java:
		content = [line.strip() for line in open(each_file)]  # list of all lines in file, each line containing a hash value

		# generate unique file identifier (shitty variable names)
		x = each_file.strip().split(".")
		z = x[1].split("/")
			
		file_val = global_id+z[3] # unique file identifier

		#print file_val # just checking
	
		# add hash codes in to our DB...
		# check if the the hash codes are not already present in the DB. If they are, append data. If not, create and store new attribute
		for word in content:
			file_val = global_id+z[3]
			words = word.strip().split('_')
			print words
			if len(words)>2:
				file_val = file_val+"#"+words[1]+"#"+words[2] 
			
				print file_val # just checking 
				java_dict[words[0]].append(file_val)
			else :
				print "Mysterious error - no line numbers generated with hash code"
				global global_errs
				global_errs+=1

	# generate a list of all words in every c file...
	for each_file in onlyfiles_c:
		content = [line.strip() for line in open(each_file)]  # list of all lines in file, each line containing a hash value

		# generate unique file identifier (shitty variable names)
		x = each_file.strip().split(".")
		z = x[1].split("/")
		
		file_val = global_id+z[3] # unique file identifier

		#print file_val # just checking
	
		# add hash codes in to our DB...
		# check if the the hash codes are not already present in the DB. If they are, append data. If not, create and store new attribute
		for word in content: 
			file_val = global_id+z[3]
			words = word.strip().split('_')
			print words
			if len(words)>2:
				file_val = file_val+"#"+words[1]+"#"+words[2] 
			
				print file_val # just checking 
				c_dict[words[0]].append(file_val)
			else :
				print "Mysterious error - no line numbers generated with hash code"
				global global_errs
				global_errs+=1

	# generate a list of all words in every cpp file...
	for each_file in onlyfiles_cpp:
		content = [line.strip() for line in open(each_file)]  # list of all lines in file, each line containing a hash value

		# generate unique file identifier (shitty variable names)
		x = each_file.strip().split(".")
		z = x[1].split("/")
		
		file_val = global_id+z[3] # unique file identifier

		#print file_val # just checking
	
		# add hash codes in to our DB...
		# check if the the hash codes are not already present in the DB. If they are, append data. If not, create and store new attribute
		for word in content: 
			file_val = global_id+z[3]
			words = word.strip().split('_')
			print words
			if len(words)>2:
				file_val = file_val+"#"+words[1]+"#"+words[2] 
			
				print file_val # just checking 
				cpp_dict[words[0]].append(file_val)
			else :
				print "Mysterious error - no line numbers generated with hash code"
				global global_errs
				global_errs+=1

	# generate a list of all words in every python file...
	for each_file in onlyfiles_py:
		content = [line.strip() for line in open(each_file)]  # list of all lines in file, each line containing a hash value

		# generate unique file identifier (shitty variable names)
		x = each_file.strip().split(".")
		z = x[1].split("/")
		print z
		file_val = global_id+z[3] # unique file identifier

		#print file_val # just checking
	
		# add hash codes in to our DB...
		# check if the the hash codes are not already present in the DB. If they are, append data. If not, create and store new attribute
		for word in content: 
			file_val = global_id+z[3]
			words = word.strip().split('_')
			print words			
			if len(words)>2:
				file_val = file_val+"#"+words[1]+"#"+words[2] 
			
				print file_val # just checking 
				py_dict[words[0]].append(file_val)
			else :
				print "Mysterious error - no line numbers generated with hash code"
				global global_errs
				global_errs+=1


	# dicts created locally, now copy these values to the DB -- Java
	t3 = time.time()
	print "Reading files, and storing hash codes in dicts took %s seconds." %(str(t3-t2))
	print "------------------------------"

	for key,value in java_dict.items():
	
		hash_val = key
		new_list = list(set(value))

		#print "Check if ", hash_val, "exists, accordingly update DB"
		#print new_list
		item = exists(hash_val, index_java)
		if item==None:
			create_new_item(hash_val, new_list, index_java)
			#print "Creating new item..."
		else:
			update_item(item, new_list, index_java)
			#print "Updating existing item..."
	print "------------------------------"	
	
	# C
	for key,value in c_dict.items():
	
		hash_val = key
		new_list = list(set(value))

		#print "Check if ", hash_val, "exists, accordingly update DB"
		#print new_list
		item = exists(hash_val, index_c)
		if item==None:
			create_new_item(hash_val, new_list, index_c)
			#print "Creating new item..."
		else:
			update_item(item, new_list, index_c)
			#print "Updating existing item..."
	print "------------------------------"

	# Cpp
	for key,value in cpp_dict.items():
	
		hash_val = key
		new_list = list(set(value))

		#print "Check if ", hash_val, "exists, accordingly update DB"
		#print new_list
		item = exists(hash_val, index_cpp)
		if item==None:
			create_new_item(hash_val, new_list, index_cpp)
			#print "Creating new item..."
		else:
			update_item(item, new_list, index_cpp)
			#print "Updating existing item..."
	print "------------------------------"		

	# Python
	for key,value in py_dict.items():
	
		hash_val = key
		new_list = list(set(value))

		#print "Check if ", hash_val, "exists, accordingly update DB"
		#print new_list
		item = exists(hash_val, index_py)
		if item==None:
			create_new_item(hash_val, new_list, index_py)
			#print "Creating new item..."
		else:
			update_item(item, new_list, index_py)
			#print "Updating existing item..."
	print "------------------------------"	

	t4 = time.time()
	print "Updating the DB took %s seconds." %(str(t4-t3))
	print "No. of errors in writing to DB - ",global_errs
			
def update_item(item, new_vals, index):
	list_val = item['file_list']
	list_val = list_val.union(set(new_vals))
	
	item['file_list'] = list_val
	item.save(overwrite=True)

# hash_val is the hash string e.g. 'ABCD'
# new_vals is a list of strings e.g. ["A", "B", "C"]
def create_new_item(hash_val, new_vals, index):
	val1 = Item(index, data={'hash_value': hash_val,'file_list': set(new_vals)})
        val1.save()
	
def exists(hash_key, index):
    	try:
        	item = index.get_item(hash_value=hash_key)
    	except boto.dynamodb2.exceptions.ItemNotFound:
        	item = None
    	return item
		
if  __name__ =='__main__':
        main()

