# Flask Imports
from flask import Flask, request, redirect, url_for, send_from_directory, Response, json, jsonify, make_response
from werkzeug.utils import secure_filename
import os, sys

# AWS Imports
import boto
import boto.s3
from boto.s3.key import Key

# Config Import
import config

# Import Algorithm API
from compare_with_DB import *

application = Flask(__name__)

# Configure the application
application.config.from_object('config.AWSConfig')
application.config.from_object('config.DevelopmentConfig')

# Connect to the bucket
conn = boto.connect_s3(application.config['AWS_ACCESS_KEY_ID'], application.config['AWS_SECRET_ACCESS_KEY'])
bucket = conn.create_bucket(application.config['AWS_BUCKET_NAME'], location=boto.s3.connection.Location.DEFAULT)
k = Key(bucket)

def percent_cb(complete, total):
    sys.stdout.write('.')
    sys.stdout.flush()

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in application.config['AWS_ALLOWED_EXTENSIONS']

# Routes for the client side
@application.route('/lib/<path:filename>', methods=['GET'])
def send_lib(filename):
    return send_from_directory('client/lib/', filename)

@application.route('/js/<path:filename>', methods=['GET'])
def send_js(filename):
    return send_from_directory('client/js/', filename)

@application.route('/partials/<path:filename>', methods=['GET'])
def send_partial(filename):
    return send_from_directory('client/partials/', filename)

@application.route('/css/<path:filename>', methods=['GET'])
def send_css(filename):
    return send_from_directory('client/assets/css/', filename)

@application.route('/img/<path:filename>', methods=['GET'])
def send_img(filename):
    return send_from_directory('client/assets/img/', filename)

@application.route('/fonts/<path:filename>', methods=['GET'])
def send_font(filename):
    return send_from_directory('client/assets/fonts/', filename)

@application.route('/bower_components/<path:filename>')
def send_bower(filename):
    return send_from_directory('bower_components/', filename)

@application.route('/favicon.ico')
def send_favicon():
    return send_from_directory('favicon.ico')

@application.route('/computestatspost/<fileType>', methods=['POST'])
def compute_stats_post(fileType):

    code = request.get_json(cache=True)

    # Save the file to newfile directory
    fo = open( "newfile/code" + "." + fileType, "wb")
    fo.write(code['body'])
    fo.close()

    #os.system("gcc -Wall -ansi -pedantic -O2 -o sig sig.c") # really?

    result = compare_db()

    fo = open("newfile/top_result.txt", "rb")
    top_resultStr = fo.read();
    #top_resultStr = str(''.join(top_resultStr))

    result[0].append(top_resultStr)
    #result = [['DAPME_Lab', 'TBWA', '/awesome/client/test/', 'app.py', 97, 1 , 10, 5, 15 ], ['DAPME_Lab', 'TBWA', '/awesome2/client/test/', 'app.py', 53, 1 , 10, 5, 15 ], ['DAPME_Lab', 'TBWA', '/awesome3/client/test/', 'app.py', 22, 1 , 10, 5, 15 ]]
    resp = Response(json.dumps({'data': result}), mimetype='application/json')
    return make_response(resp)


@application.route('/', methods=['GET'])
def home():
    """ Return the html seed file with linked JS """
    with open('client/index.html') as base:
        return make_response(base.read())

if __name__ == '__main__':
	application.run(host='0.0.0.0', debug=True)
