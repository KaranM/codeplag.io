import sys,os
import subprocess
from subprocess import check_output
from subprocess import Popen, PIPE

def main():
	make_files()

def dosig(thisfile,path,save_path,name):
	pipe = Popen(["./sig", thisfile], stdout=PIPE)
	out = pipe.communicate()[0]
	pathname = path.strip().split('/')
	if len(pathname)>2 :
		first = pathname[2]
	else:
		first = ''
	pathname = pathname[3:]
	print pathname
	fname = '&'.join(pathname)
	fname = first+'&'+fname
	print fname
	thisfile_output = fname+'&'+name+".sig"
	completeName = os.path.join(save_path, thisfile_output)

	op = open (completeName, 'wb')
	op.write(out)
	op.close()

def make_files():
	root = "./output"
	os.system("gcc -Wall -ansi -pedantic -O2 -o sig sig.c") 
	for path, subdirs, files in os.walk(root):
	    for name in files:
		thisfile = os.path.join(path, name)
		#out = check_output(["./sig", thisfile])
		if name.endswith(".java"):
			save_path = "./sig_files/java"
			dosig(thisfile,path,save_path,name)
		elif name.endswith(".c"):
			save_path = "./sig_files/c"
			dosig(thisfile,path,save_path,name)
		elif name.endswith(".cpp") or name.endswith(".cc"):
			save_path = "./sig_files/cpp"
		        dosig(thisfile,path,save_path,name)
		elif name.endswith(".py"):
			save_path = "./sig_files/py"
		        dosig(thisfile,path,save_path,name)
	
if  __name__ =='__main__':
        main()

