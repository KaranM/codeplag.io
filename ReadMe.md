# CodePlag.io

The most robust cloud computing platform for finding instances of code plagiarism. This project was created for learning purposes as a part of the Cloud Computing and Big Data course at Columbia University under the tutelage of Professor Sambit Sahu. COMSW6998, Spring 2014.

## Running the Application Locally

1. Change to the src directory:

		cd src/

2. Install venv on Windows or Mac (either command):

		sudo pip install virtualenv
	
	or
	
		sudo easy_install virtualenv

	For Ubuntu: 

		sudo apt-get install python-virtualenv


	If you do not have pip, please see how to install pip: http://pip.readthedocs.org/en/latest/installing.html

3. Start a virtual environment initialized with all necessary dependencies (specified by requirements.txt). Fortunately, python flask has venv, which makes this step as easy as: 

		. venv/bin/activate

4. Run the web application within the virtual environment
		
		python application.py

5. You are done! Go to localhost:5000 to interface with the web application

## What is the Frontend?

* Angular JS, which runs an MVC on the front end components
* Additional Javascript
* HTML/CSS/Bootstrap
* CodeMirror

## What is the Backend?

* Python flask, which runs an MVC, interfacing with the frontend and all Amazon Web Services
* AWS Services ysed: EC2, S3, DynamoDB
* Keys for AWS are configured in config.py