'use strict';

/* Filters */

angular.module('myApp.filters', []).
filter('interpolate', ['version', function(version) {
return function(text) {
  return String(text).replace(/\%VERSION\%/mg, version);
}
}]);

angular.module('myApp.filters', []).
filter('makeRange', function() {
    return function(input) {
        var lowBound, highBound;
        switch (input.length) {
        case 1:
            lowBound = 0;
            highBound = parseInt(input[0]) - 1;
            break;
        case 2:
            lowBound = parseInt(input[0]);
            highBound = parseInt(input[1]);
            break;
        default:
            return input;
        }
        var result = [];
        for (var i = lowBound; i <= highBound; i++)
            result.push(i);
        return result;
    };
});

angular.module('myApp.filters', [])
.filter('escape', function() {
  //return window.escape;
  return window.encodeURI;
  /*
    return function(text) {
        return text
                //.replace(/\//g, '&#47')
                .replace(/\//g, '/')
                //.replace(/:/g, '&#58');
    }
    */
});