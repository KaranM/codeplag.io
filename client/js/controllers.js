'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
    .controller('DashboardController', ['$scope', '$http', function($scope, $http) {

        $scope.masthead_header = "Show us your code!";
        $scope.masthead_text = "What are you afraid of?";
        
        $scope.codeText = 'PUT YOUR CODE HERE';

        $scope.editorOptions = {
            lineWrapping : true,
            lineNumbers: true,
            mode: 'python',
            theme: 'twilight'
        };

        $scope.language = ['py','java','c','cpp'];
        $scope.currentLanguage = 'py'; // Initialized to python


        $scope.computeStatsForCode = function() {
            $scope.statTitle = "Statistics.";

            var data = {};
            data['body'] = $scope.codeText;

            $http.post( '/computestatspost/' + $scope.currentLanguage, data)
            .success( function(response) {
                $scope.data = response['data'];
                $scope.stats = [];

                for (var i = 0; i < $scope.data.length; i++) {
                    $scope.stats[i] = $scope.data[i][2];
                }

            });
        }

        $scope.changeLanguage = function(index) {
            // Change the current language
            $scope.currentLanguage = $scope.language[index];
            if (index == 0) {
                $scope.editorOptions.mode = 'python';
            } else {
                $scope.editorOptions.mode = 'clike';
            }
        }
        


    }])
    .controller('AboutController', ['$scope', function($scope) {

        $scope.masthead_header = "About";
        $scope.masthead_text = "Who? What? Why?";

    }])
    .controller('ContactController', ['$scope', function($scope) {

        $scope.masthead_header = "Contact";
        $scope.masthead_text = "Feel free to inquire about the project. We are nice people.";

    }])
    .controller('HomeController', ['$scope', function($scope) {

        $scope.masthead_header = "Check code for plagiarism";
        $scope.masthead_text = "The most robust cloud computing platform for finding instances of code plagiarism.";

        $scope.items = [];
  
        $scope.push = function() {
            $scope.items.push("awesome");
        };

        $scope.pop = function() {
            $scope.items.pop();
        };

    }]);
