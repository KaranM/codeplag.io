'use strict';

// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'ui.codemirror',
  'ui.ace'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {templateUrl: 'partials/home.html', controller: 'HomeController'});
  $routeProvider.when('/about', {templateUrl: 'partials/about.html', controller: 'AboutController'});
  $routeProvider.when('/contact', {templateUrl: 'partials/contact.html', controller: 'ContactController'});
  $routeProvider.when('/getting-started', {templateUrl: 'partials/dashboard.html', controller: 'DashboardController'});
  $routeProvider.otherwise({redirectTo: '/'});
}]);
