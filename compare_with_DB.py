'''
Reads in a single file containing intermediate code representation
Creates hash codes for this file
Compares these hash codes against the ones already present in the DynamoDB table (created earlier)
Computes similarity score with top 3 matches
Returns - top 3 similar files with correponding line numbers in the form of list of lists :--
[[github_URL, filepath_in_repo, similarity_score]..]
'''
import boto.dynamodb2
from boto.dynamodb2.fields import HashKey, RangeKey, KeysOnlyIndex, AllIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.items import Item
import nltk
from os import listdir
from os.path import isfile, join
from collections import defaultdict
import time
import sys,os
import subprocess
from subprocess import check_output
from subprocess import Popen, PIPE

# tables are global variable... yeah...
index_c = Table('index_c')
index_cpp = Table('index_cpp')
index_java = Table('index_java')
index_py = Table('index_python')

def main():
	match = compare_db()
	print "Matches"
	print match  

## Returns a list of the top match as follows -- [gitrepoURL, path_to_file_in_repo, similarity_score]
def compare_db():
	os.system("gcc -Wall -ansi -pedantic -O2 -o sig sig.c") 
	t1 = time.time()
	# get the new query file
	mypath =  './newfile/' # specify path to query file here. 
	onlyfiles = [ join(mypath,f) for f in listdir(mypath) if isfile(join(mypath,f)) ] # contains all files given by user. For now, just 1
	
	inp_file = onlyfiles[0] # assumes only one file. Put in a loop if an entire directory is passed as input
	extension = ''
	# generate ./sig file for this :
	pipe = Popen(["./sig", inp_file], stdout=PIPE)
	out = pipe.communicate()[0]
	
	#print out # just checking
	words = out.strip().split('\n')
	#print words  # each of these is in the format : hashcode_beginline_endline 	

	matches = defaultdict(list)
	matching_lines = defaultdict(list)
	results = []
	
	t2 = time.time()
	if inp_file.endswith(".py"):
		DB = index_py
		extension = '.py'
	elif inp_file.endswith(".java"):
		DB = index_java
		extension = '.java'
	elif inp_file.endswith(".c"):
		DB = index_c
		extension = '.c'
	elif inp_file.endswith(".cpp"):
		DB = index_cpp
		extension = '.cpp'
	elif inp_file.endswith(".cc"):
		DB = index_cpp
		extension = '.cc'	
	else :
		print "Cannot handle this file. Stahp."  # the rest of the code will just throw errors. Yes, my error handling is abysmal. Will fix.

	print "Preliminary code took %s seconds." %(str(t2-t1))
	
	no_of_words = len(words)
	print no_of_words
	no_of_matches = 0
	if no_of_words>1:
		# for each hash value, find the key,value pair in the DB table
		for word in words:
			#print gram
			this_word = word.strip().split('_')
			hashval = this_word[0]
			line1 = word[1]
			line2= word[2]
			#print hashval, line1, line2
			item = exists(hashval, DB)
			if item!=None :		
				dup_matches = item['file_list']
				no_of_matches +=1
				#print dup_matches
				matches[word].append(dup_matches)

		#print matches	# all the matches we found
		t3 = time.time()
		print "DB matching took %s seconds." %(str(t3-t2))
		dict_matches=defaultdict(int)
		#print "@#$%^&*()#$%^&*()"

		for key, matchlist in matches.items():
			for value in matchlist :
				#print value
				actual_val = list(value)[0]
				val_list = actual_val.strip().split('#')
				#print val_list
				dict_matches[val_list[0]] += 1
				newpair = [key, str(actual_val)]
				matching_lines[val_list[0]].append(newpair) 

		print "-----------------------"

		flag = 0 # flag that checks for a match
		count = 0 # counter for number of results we want
		
		topkey = ''
		toprepo = ''
		for w in sorted(dict_matches, key=dict_matches.get, reverse=True):
	  		value = dict_matches[w]
			key = w
			count+=1
			sim = (float(value)/no_of_words*100)
			if sim>100.0:
				sim = 100.0
			if count==1:
				topkey=key
			print "Similarity with ", key, " is - ", sim, "%"
			key = key.replace('&','/')
			key = key.split('/')
			#print key
			username = key[0] 
			print username
			reponame = key[1] 
			print reponame
			if count==1:
				toprepo=reponame
			pathname = key[2:]
			pathlist = ['/'.join(pathname)]
			filename = pathlist[0]+extension
			print filename
			topmatch = []
			gitrepo = "github.com/"+username+"/"+reponame+".git"
			topmatch.append(gitrepo)
			topmatch.append(filename)
			topmatch.append(str.format('{0:.2f}',sim))
			results.append(topmatch)
			if count==1:
				break
		# Here some accuracy value must be generated
		print "-----------------------"
		t4 = time.time()
		print "Accuracy calculation took %s seconds." %(str(t4-t3))
		
		top_file = results[0]
		top_line_matches = matching_lines[topkey]
		#print top_file
		#print top_line_matches
	
	## clone git repo, find the right file, store it in the right place
	repoURL = str(results[0][0])
	os.system("rm newfile/top_result.txt")	
	os.system("git clone https://" + repoURL + " result_repo/"+toprepo)
	os.system("cp result_repo/"+str(results[0][1])+" newfile/top_result.txt")
	os.system("rm -rf result_repo")
	
	return results
			
def exists(hash_key, index):
    	try:
        	item = index.get_item(hash_value=hash_key)
    	except boto.dynamodb2.exceptions.ItemNotFound:
        	item = None
    	return item
		
if  __name__ =='__main__':
        main()

